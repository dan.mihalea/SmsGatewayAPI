using Microsoft.EntityFrameworkCore;
using Models.SmsGateway.ContactModel;
using SmsGatewayAPI.Models.Menu;
using System.Security.Cryptography.X509Certificates;

namespace Models.Context
{
    public class TikiTakaSmsContext : DbContext
    {
        public TikiTakaSmsContext(DbContextOptions<TikiTakaSmsContext> opt) : base(opt)
        {

        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<List> Lists { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<MenuCategory> MenuCategories { get; set; }
        public DbSet<MenuIngredient> MenuIngredients { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ContactList>(cl =>
            {
                cl.HasKey(x => x.Id);
                cl.Property(x => x.Id).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<ContactList>()
                .HasOne(cl => cl.Contact)
                .WithMany(cl => cl.Lists)
                .HasForeignKey(cl => cl.ContactId);
            modelBuilder.Entity<ContactList>()
                .HasOne(cl => cl.List)
                .WithMany(cl => cl.Contacts)
                .HasForeignKey(cl => cl.ListId);

            modelBuilder.Entity<MenuItemIngredient>()
                .HasOne(mii => mii.MenuItem)
                .WithMany(mii => mii.MenuIngredients)
                .HasForeignKey(mii => mii.MenuItemId);
            modelBuilder.Entity<MenuItemIngredient>()
                .HasOne(mii => mii.MenuIngredient)
                .WithMany(mii => mii.MenuItems)
                .HasForeignKey(mii => mii.MenuIngredientId);


            modelBuilder.Entity<MenuItem>()
                .HasOne(mi => mi.MenuCategory)
                .WithMany(mi => mi.MenuItems)
                .HasForeignKey(mi => mi.MenuCategoryId);

        }
    }
}