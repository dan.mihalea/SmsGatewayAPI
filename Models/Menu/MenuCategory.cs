﻿using SmsGatewayAPI.Models.Common;
using System.Collections.Generic;

namespace SmsGatewayAPI.Models.Menu
{
    public class MenuCategory : Base
    {
        public string Name { get; set; }
        public List<MenuItem> MenuItems { get; set; }
    }
}
