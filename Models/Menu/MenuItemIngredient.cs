﻿using SmsGatewayAPI.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Models.Menu
{
    public class MenuItemIngredient : Base
    {
        public int MenuItemId { get; set; }
        public int MenuIngredientId { get; set; }
        public MenuItem MenuItem { get; set; }
        public MenuIngredient MenuIngredient { get; set; }
        public int? Weight { get; set; }
    }
}
