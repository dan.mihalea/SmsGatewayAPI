﻿using SmsGatewayAPI.Models.Common;
using System.Collections.Generic;

namespace SmsGatewayAPI.Models.Menu
{
    public class MenuItem : Base
    {        
        public string Name { get; set; }
        public string Description { get; set; }
        public Price ItemPrice { get; set; }
        public IList<MenuItemIngredient> MenuIngredients { get; set; }
        
        // navigation properties
        public MenuCategory MenuCategory { get; set; }
        public int MenuCategoryId { get; set; }
    }
}
