﻿using SmsGatewayAPI.Models.Common;
using SmsGatewayAPI.Services.Menu;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Models.Menu
{
    public class MenuIngredient : Base
    {        
        public string Name { get; set; }
        public bool IsAllergen { get; set; }

        //navigation properties
        public IList<MenuItemIngredient> MenuItems { get; set; }        
    }
}
