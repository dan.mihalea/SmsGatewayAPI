﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RestAPI.Models.SmsGateway.LogoutModel
{
    [XmlRoot("request")]
    public class LogoutRequest
    {
        public int Logout { get; set; }
    }
}
