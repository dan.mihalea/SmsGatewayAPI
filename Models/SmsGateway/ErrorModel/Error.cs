﻿namespace RestAPI.Models.SmsGateway.ErrorModel
{
    public class Error
    {
        public long Code { get; set; }
        public string Message { get; set; }
    }
}
