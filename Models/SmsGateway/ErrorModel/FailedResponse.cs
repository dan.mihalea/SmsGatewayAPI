
using RestAPI.Models.SmsGateway.ErrorModel;

namespace Models.SmsGateway.ErrorModel
{
    public class FailedResponse : IHuaweiResponse
    {
        public Error Error { get; set; }
    }
}