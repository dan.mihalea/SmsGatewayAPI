namespace Models.SmsGateway.ErrorModel
{
    public class SuccessResponse : IHuaweiResponse
    {
        public string Response { get; set; }
    }
}