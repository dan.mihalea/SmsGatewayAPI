﻿using System.Xml.Serialization;

namespace RestAPI.Models.SmsGateway
{
    [XmlRoot("request")]
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
