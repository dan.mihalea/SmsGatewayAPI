using System;
using System.Collections.Generic;

namespace Models.SmsGateway.ContactModel
{
    public class List
    {
        public long Id { get; set; }
        public string ListName { get; set; }
        public DateTime Created { get; set; }
        public List<ContactList> Contacts { get; set; }
    }
}