using System.Collections.Generic;

namespace Models.SmsGateway.ContactModel
{
    public class Contact
    {
        public long Id { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public List<ContactList> Lists { get; set; }
    }
}