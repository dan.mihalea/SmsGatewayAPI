using System;

namespace Models.SmsGateway.ContactModel
{
    public class ContactList
    {
        public long Id { get; set; }
        public long ListId { get; set; }
        public long ContactId { get; set; }
        public DateTime Created { get; set; }
        public Contact Contact { get; set; }
        public List List { get; set; }
    }
}