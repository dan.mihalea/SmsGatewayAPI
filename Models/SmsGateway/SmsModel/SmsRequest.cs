using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Models.SmsGateway.SmsModel
{
    [XmlRoot("request")]
    public class SmsRequest
    {
        public int Index { get; set; }
        [XmlArrayItem("Phone")]
        public List<string> Phones { get; set; }
        public string Sca { get; set; }
        public string Content { get; set; }
        public long Length { get; set; }
        public int Reserved { get; set; }
        public string Date { get; set; }

        public void PrepareSerialization()
        {
            Index = -1;
            Sca = "";
            Length = Content.Length;
            Reserved = 1;
            Date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}