﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Models.SmsGateway.SmsModel
{
    public class SmsListRequest
    {
        public List<long> ListIds { get; set; }
        public string Message { get; set; }
    }
}
