﻿using SmsGatewayAPI.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Models.DTO.Menu
{
    public class MenuCategoryDTO : Base
    {
        public string Name { get; set; }
    }
}
