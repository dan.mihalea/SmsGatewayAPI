﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Models.Common
{
    public class Currency : Base
    {
        public string Name { get; set; }
        public string ISOName { get; set; }
    }
}
