﻿using SmsGatewayAPI.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Models.Common
{
    public class Price : Base
    {
        public int Value { get; set; }
        public Currency Currency { get; set; }
    }
}
