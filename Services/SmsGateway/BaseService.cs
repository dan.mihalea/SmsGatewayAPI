using Models.SmsGateway;
using Models.SmsGateway.ErrorModel;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Services.SmsGateway
{
    public class BaseService
    {
        public const string baseUrl = "http://www.huaweimobilewifi.com/api";

        public HttpContent CreateXmlContent(object content)
        {
            HttpContent httpContent = null;

            if (content != null)
            {
                XmlSerializer xmlSerializer = new XmlSerializer(content.GetType());
                
                var noNamespace = new XmlSerializerNamespaces();                
                using (StringWriter sw = new StringWriter())
                {                    
                    XmlWriterSettings settings = new XmlWriterSettings();                    
                    xmlSerializer.Serialize(sw, content);
                    httpContent = new StringContent(sw.ToString(), Encoding.UTF8, "application/xml");
                }
            }
            return httpContent;
        }

        public async Task<IHuaweiResponse> SendAsync(Uri hostUri, object payload)
        {
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = hostUri,
                    Content = CreateXmlContent(payload),
                };
                request.Headers.Add("Accept", "application/xml");
                var response = await client.SendAsync(request);

                var xmlResponse = await response.Content.ReadAsStringAsync();
                var parsedXml = new XmlDocument();
                parsedXml.LoadXml(xmlResponse);
                xmlResponse = JsonConvert.SerializeXmlNode(parsedXml);

                if (parsedXml.SelectNodes("/error").Count > 0)
                {
                    return JsonConvert.DeserializeObject<FailedResponse>(xmlResponse);
                }
                else
                {
                    return JsonConvert.DeserializeObject<SuccessResponse>(xmlResponse);
                }

            }
        }

    }
}