using System.Xml;
using System.Net.Http;
using System.Threading.Tasks;

using Newtonsoft.Json;
using System;
using Models.SmsGateway;
using Models.SmsGateway.ErrorModel;
using RestAPI.Models.SmsGateway;
using RestAPI.Models.SmsGateway.LogoutModel;

namespace Services.SmsGateway.User
{
    public class UserService : BaseService, IUserService
    {
        public UserService() : base()
        {

        }
        public async Task<IHuaweiResponse> Login(LoginRequest loginRequest)
        {
            IHuaweiResponse responseBody;

            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{baseUrl}/user/login"),
                    Content = CreateXmlContent(loginRequest),
                };
                request.Headers.Add("Accept", "application/xml");
                var response = await client.SendAsync(request);

                var xmlResponse = await response.Content.ReadAsStringAsync();
                var parsedXml = new XmlDocument();
                parsedXml.LoadXml(xmlResponse);
                xmlResponse = JsonConvert.SerializeXmlNode(parsedXml);

                if (parsedXml.SelectNodes("/error").Count > 0)
                {
                    responseBody = JsonConvert.DeserializeObject<FailedResponse>(xmlResponse);
                }
                else
                {
                    responseBody = JsonConvert.DeserializeObject<SuccessResponse>(xmlResponse);
                }

            }

            return responseBody;
        }

        public async Task<IHuaweiResponse> Logout()
        {
            LogoutRequest logoutRequest = new LogoutRequest { Logout = 1 };
            return await SendAsync(new Uri($"{baseUrl}/User/logout"), logoutRequest);
        }
    }
}