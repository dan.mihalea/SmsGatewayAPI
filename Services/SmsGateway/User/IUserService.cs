using System.Collections.Generic;
using System.Threading.Tasks;
using Models.SmsGateway;
using RestAPI.Models.SmsGateway;

namespace Services.SmsGateway.User
{
    public interface IUserService
    {
        Task<IHuaweiResponse> Login(LoginRequest loginRequest);
        Task<IHuaweiResponse> Logout();

    }
}