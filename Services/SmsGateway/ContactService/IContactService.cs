using System.Collections.Generic;
using System.Threading.Tasks;
using Models.SmsGateway.ContactModel;

namespace Services.SmsGateway.ContactService
{
    public interface IContactService
    {
        Task<List<Contact>> GetAll();
        Task AddContact(Contact contact);
        Task RemoveContacts(List<long> contacts);
    }
}