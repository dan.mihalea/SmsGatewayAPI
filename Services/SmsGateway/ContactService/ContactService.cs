using System.Xml.Schema;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.SmsGateway.ContactModel;
using Repositories.ContactRepo;
using System.Linq;

namespace Services.SmsGateway.ContactService
{
    public class ContactService : IContactService
    {
        private readonly IContactRepo _contactRepo;

        public ContactService(IContactRepo contactRepo)
        {
            _contactRepo = contactRepo;
        }

        public async Task AddContact(Contact contact)
        {
            await _contactRepo.AddContact(contact);
        }

        public async Task<List<Contact>> GetAll()
        {
            return await _contactRepo.GetAll();
        }

        public async Task RemoveContacts(List<long> contacts)
        {
            var allContacts = await _contactRepo.GetAll();
            var toRemove = allContacts.Where(x => contacts.Any(y => y == x.Id)).ToList();
            await _contactRepo.RemoveContacts(toRemove);
        }
    }
}