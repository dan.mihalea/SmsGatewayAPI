﻿using Models.SmsGateway.ContactModel;
using RestAPI.Repositories.ListRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace RestAPI.Services.SmsGateway.ListService
{
    public class ListService : IListService
    {
        private IListRepo _listRepo;
        public ListService(IListRepo listRepo)
        {
            _listRepo = listRepo;
        }

        public async Task AddList(List newList)
        {
            await _listRepo.AddList(newList);
        }

        public async Task AddUserToList(long listId, List<long> users)
        {
            await _listRepo.AddUserToList(listId, users);
        }

        public async Task<List<List>> GetAll()
        {
            return await _listRepo.GetAll();
        }

        public async Task<List> GetListById(long listId)
        {
            return await _listRepo.GetListById(listId);
        }

        public async Task RemoveLists(List<long> listId)
        {
            var allLists = await _listRepo.GetAll();
            var toRemove = allLists.Where(x => listId.Any(y => y == x.Id)).ToList();
            await _listRepo.RemoveLists(toRemove);
        }
    }
}
