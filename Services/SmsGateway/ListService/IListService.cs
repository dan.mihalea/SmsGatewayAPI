﻿using Models.SmsGateway.ContactModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RestAPI.Services.SmsGateway.ListService
{
    public interface IListService
    {
        Task<List<List>> GetAll();
        Task AddList(List newList);
        Task<List> GetListById(long listId);
        Task AddUserToList(long listId, List<long> users);
        Task RemoveLists(List<long> listId);
    }
}
