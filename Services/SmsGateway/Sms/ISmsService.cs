using System.Collections.Generic;
using System.Threading.Tasks;
using Models.SmsGateway;
using Models.SmsGateway.SmsModel;
using SmsGatewayAPI.Models.SmsGateway.SmsModel;

namespace Services.SmsGateway.Sms
{
    public interface ISmsService
    {
        Task<IHuaweiResponse> SendSms(SmsRequest request);
        Task<SmsRequest> PrepareMessage(SmsListRequest smsListRequest);
    }
}