using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using Models.SmsGateway;
using Models.SmsGateway.ContactModel;
using Models.SmsGateway.ErrorModel;
using Models.SmsGateway.SmsModel;
using Newtonsoft.Json;
using RestAPI.Services.SmsGateway.ListService;
using SmsGatewayAPI.Models.SmsGateway.SmsModel;

namespace Services.SmsGateway.Sms
{
    public class SmsService : BaseService, ISmsService
    {
        private readonly IListService _listService;
        public SmsService(IListService listService) : base()
        {
            _listService = listService;
        }

        public async Task<SmsRequest> PrepareMessage(SmsListRequest smsListRequest)
        {
            var allLists = await _listService.GetAll();
            var messageLists = allLists.Where(x => smsListRequest.ListIds.Any(l => l == x.Id)).ToList();
            var phoneNumbers = new List<string>();
            foreach (var item in messageLists)
            {
                foreach (var contact in item.Contacts)
                {
                    if (!phoneNumbers.Any(x => x == contact.Contact.PhoneNumber))
                    {
                        phoneNumbers.Add(contact.Contact.PhoneNumber);
                    }
                }
            }
            var result = new SmsRequest
            {
                Phones = phoneNumbers,
                Content = smsListRequest.Message
            };

            return result;
        }

        public async Task<IHuaweiResponse> SendSms(SmsRequest smsRequest)
        {
            smsRequest.PrepareSerialization();

            IHuaweiResponse responseBody;
            using (var client = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{baseUrl}/sms/send-sms"),
                    Content = CreateXmlContent(smsRequest)
                };

                request.Headers.Add("Accept", "application/xml");
                var response = await client.SendAsync(request);

                var xmlResponse = await response.Content.ReadAsStringAsync();
                var parsedXml = new XmlDocument();
                parsedXml.LoadXml(xmlResponse);
                xmlResponse = JsonConvert.SerializeXmlNode(parsedXml);

                if (parsedXml.SelectNodes("/error").Count > 0)
                {
                    responseBody = JsonConvert.DeserializeObject<FailedResponse>(xmlResponse);
                }
                else
                {
                    responseBody = JsonConvert.DeserializeObject<SuccessResponse>(xmlResponse);
                }
            }
            return responseBody;
        }
    }
}