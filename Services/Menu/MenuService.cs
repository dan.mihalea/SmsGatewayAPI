﻿using AutoMapper;
using SmsGatewayAPI.Models.DTO.Menu;
using SmsGatewayAPI.Models.Menu;
using SmsGatewayAPI.Repositories.MenuRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Services.Menu
{
    public class MenuService : IMenuService
    {
        private readonly IMenuRepository _menuRepository;
        private readonly IMapper _mapper;

        public MenuService(IMenuRepository menuRepository, IMapper mapper)
        {
            _menuRepository = menuRepository;
            _mapper = mapper;
        }

        public async Task<IList<MenuIngredient>> GetAllergens()
        {
            return await Task.FromResult(new List<MenuIngredient>());
        }

        public async Task<IList<MenuCategoryDTO>> GetMenuCategories()
        {
            var result = await _menuRepository.GetCategories();
            return _mapper.Map<IList<MenuCategoryDTO>>(result);
        }
    }
}
