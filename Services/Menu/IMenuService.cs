﻿using SmsGatewayAPI.Models.DTO.Menu;
using SmsGatewayAPI.Models.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Services.Menu
{
    public interface IMenuService
    {
        Task<IList<MenuIngredient>> GetAllergens();
        Task<IList<MenuCategoryDTO>> GetMenuCategories();
    }
}
