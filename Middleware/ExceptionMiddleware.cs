﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using SmsGatewayAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;        

        public ExceptionMiddleware(RequestDelegate next)
        {            
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {                
                await HandleExceptionAsync(context, ex);
            }
        }
        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            ErrorDetails response = new ErrorDetails
            {
                Message = exception.Message,
                StatusCode = context.Response.StatusCode
            };

            return context.Response.WriteAsync(response.ToString());
        }
    }
}
