using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Models.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Repositories.ContactRepo;
using RestAPI.Repositories.ListRepo;
using RestAPI.Services.SmsGateway.ListService;
using Services.SmsGateway.ContactService;
using Services.SmsGateway.Sms;
using Services.SmsGateway.User;
using SmsGatewayAPI.Extensions;
using SmsGatewayAPI.Repositories.MenuRepo;
using SmsGatewayAPI.Services.Menu;

namespace RestAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddDbContext<TikiTakaSmsContext>(opt =>
                opt.UseSqlServer(Configuration.GetConnectionString("TikiTakaSms"))
            );
            services.AddCors();
            services
                .AddControllers()
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddHttpClient();
            
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ISmsService, SmsService>();
            services.AddScoped<IListService, ListService>();
            services.AddScoped<IListRepo, ListRepo>();
            services.AddScoped<IContactService, ContactService>();
            services.AddScoped<IContactRepo, ContactRepo>();
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IMenuRepository, MenuRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureExceptionMiddleware();

            app.UseCors(
                options =>
                {
                    options.WithOrigins("*").AllowAnyMethod();
                    options.WithHeaders("*").AllowAnyHeader();
                }
            );

            //app.UseHttpsRedirection();
            app.UseSerilogRequestLogging();
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
