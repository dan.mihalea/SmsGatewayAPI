using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.SmsGateway.ContactModel;
using Services.SmsGateway.ContactService;

namespace Controllers.SmsMachine
{
    [Route("api/contact")]
    public class ContactController : ControllerBase
    {
        private readonly IContactService _contactService;

        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var result = await _contactService.GetAll();
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> AddContact([FromBody] Contact contact)
        {
            var existingContact = (await _contactService.GetAll()).Any(x => x.PhoneNumber == contact.PhoneNumber);
            
            if (!existingContact)
            {
                await _contactService.AddContact(contact);
                return NoContent();
            }
            else
            {
                return BadRequest(new { errorMessage = $"Entry with phone number {contact.PhoneNumber} already exists in the database" });
            }
        }

        [Route("remove")]
        [HttpPost]
        public async Task<IActionResult> RemoveUsers([FromBody] List<long> contacts)
        {
            if (contacts.Count == 0) return BadRequest(new { errorMessage = "No contact selected for deletion" });
            await _contactService.RemoveContacts(contacts);

            return NoContent();
        }
    }
}