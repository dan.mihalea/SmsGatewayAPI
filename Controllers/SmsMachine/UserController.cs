using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.SmsGateway.ErrorModel;
using RestAPI.Models.SmsGateway;
using Serilog;
using Services.SmsGateway.User;

namespace Controllers.SmsMachine
{
    [ApiController]
    [Route("api/user")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            var result = await _userService.Login(loginRequest);
            if (result is FailedResponse)
            {
                string errorMessage = ((FailedResponse)result).Error.Code == 108006 ? "Incorrect credentials" : "Failed to connect to the device. Please try again!";
                Log.Error(errorMessage);
                return StatusCode(500, new { errorMessage });
            }
            return Ok(result);
        }
        [HttpPost]
        [Route("logout")]
        public async Task<ActionResult> Logout()
        {
            var result = await _userService.Logout();
            return Ok(result);
        }
    }
}