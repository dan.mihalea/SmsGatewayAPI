using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.SmsGateway.ContactModel;
using Models.SmsGateway.SmsModel;
using RestAPI.Services.SmsGateway.ListService;
using Services.SmsGateway.Sms;
using SmsGatewayAPI.Models.SmsGateway.SmsModel;

namespace Controllers.SmsMachine
{
    [ApiController]
    [Route("api/sms")]
    public class SmsController : ControllerBase
    {
        private readonly ISmsService _smsService;

        public SmsController(ISmsService smsService)
        {
            _smsService = smsService;
        }
        [HttpPost]
        [Route("send-sms")]
        public async Task<ActionResult> SendSms(SmsRequest smsRequest)
        {
            var result = await _smsService.SendSms(smsRequest);
            return Ok(result);
        }
        [HttpPost]
        [Route("list-sms")]
        public async Task<IActionResult> SendListSms([FromBody] SmsListRequest listMessageRequest)
        {
            var smsRequest = await _smsService.PrepareMessage(listMessageRequest);
            var result = await SendSms(smsRequest);
            return Ok(result);

        }
    }
}