﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models.SmsGateway.ContactModel;
using RestAPI.Services.SmsGateway.ListService;

namespace Controllers.SmsMachine
{
    [Route("api/[controller]")]
    [ApiController]
    public class ListController : ControllerBase
    {
        public IListService _listService;
        public ListController(IListService listService)
        {
            _listService = listService;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _listService.GetAll();
            return Ok(result);
        }
        [HttpPost]
        public async Task<ActionResult> AddList([FromBody] List newList)
        {
            await _listService.AddList(newList);
            return CreatedAtRoute(nameof(GetListById), new { listId = newList.Id }, newList);
        }
        [Route("{listId}", Name = "GetListById")]
        [HttpGet]
        public async Task<IActionResult> GetListById([FromRoute] long listId)
        {
            var result = await _listService.GetListById(listId);
            if (result != null)
                return Ok(result);
            return NotFound(new { errorMessage = $"The list could not be found" });
        }

        [Route("{listId}/addUsers")]
        [HttpPost]
        public async Task<IActionResult> AddUserToList([FromBody] List<long> users, [FromRoute] long listId)
        {
            var existingList = await _listService.GetListById(listId);
            if (existingList == null) return NotFound(new { errorMessage = $"The list could not be found" });

            await _listService.AddUserToList(listId, users);
            return CreatedAtRoute(nameof(GetListById), new { listId = existingList.Id }, existingList);
        }
        [Route("remove")]
        [HttpPost]
        public async Task<IActionResult> RemoveList([FromBody] List<long> listId)
        {
            await _listService.RemoveLists(listId);
            return NoContent();
        }
    }
}
