﻿using Microsoft.AspNetCore.Mvc;
using SmsGatewayAPI.Services.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Controllers.Menu
{
    [Route("api/{controller}")]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService _menuService;
        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }
    }
}