﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SmsGatewayAPI.Services.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Controllers.Categories
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly IMenuService _menuService;

        public CategoriesController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        [HttpGet]        
        public async Task<IActionResult> GetMenuCategories()
        {
            var result = await _menuService.GetMenuCategories();            
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddCategory()
        {
            throw new NotImplementedException();
        }
    }
}
