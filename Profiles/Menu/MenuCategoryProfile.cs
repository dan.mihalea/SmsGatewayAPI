﻿using AutoMapper;
using SmsGatewayAPI.Models.DTO.Menu;
using SmsGatewayAPI.Models.Menu;

namespace SmsGatewayAPI.Profiles.Menu
{
    public class MenuCategoryProfile : Profile
    {
        public MenuCategoryProfile()
        {
            CreateMap<MenuCategory, MenuCategoryDTO>();
        }
        
    }
}
