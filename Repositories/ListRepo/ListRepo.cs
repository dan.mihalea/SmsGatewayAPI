﻿using Microsoft.EntityFrameworkCore;
using Models.Context;
using Models.SmsGateway.ContactModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace RestAPI.Repositories.ListRepo
{
    public class ListRepo : IListRepo
    {
        private readonly TikiTakaSmsContext _context;
        public ListRepo(TikiTakaSmsContext context)
        {
            _context = context;
        }

        public async Task AddList(List newList)
        {
            await _context.Lists.AddAsync(newList);
            await _context.SaveChangesAsync();
        }

        public async Task AddUserToList(long listId, List<long> users)
        {
            var existingList = await _context.Lists
                .Include("Contacts")
                .FirstOrDefaultAsync(x => x.Id == listId);
            var contacts = await _context.Contacts.Where(x => users.Any(y => y == x.Id)).ToListAsync();
            var contactLists = new List<ContactList>();
            foreach (var item in contacts)
            {
                var existingEntry = existingList.Contacts.Any(x => x.ListId == listId && x.ContactId == item.Id);
                if (!existingEntry)
                {
                    contactLists.Add(new ContactList
                    {
                        ListId = listId,
                        ContactId = item.Id,
                        Created = DateTime.Now
                    });
                }
            }
            existingList.Contacts.AddRange(contactLists);
            await _context.SaveChangesAsync();
        }

        public async Task<List<List>> GetAll()
        {
            return await _context.Lists
                .Include(x => x.Contacts)
                .ThenInclude(x => x.Contact)
                .ToListAsync();
        }

        public async Task<List> GetListById(long listId)
        {
            return await _context.Lists.FirstOrDefaultAsync(x => x.Id == listId);
        }

        public async Task RemoveLists(List<List> listId)
        {
            _context.Lists.RemoveRange(listId);
            await _context.SaveChangesAsync();
        }
    }
}
