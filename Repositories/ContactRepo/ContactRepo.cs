using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Models.Context;
using Models.SmsGateway.ContactModel;

namespace Repositories.ContactRepo
{
    public class ContactRepo : IContactRepo
    {
        private readonly TikiTakaSmsContext _ctx;

        public ContactRepo(TikiTakaSmsContext ctx)
        {
            _ctx = ctx;
        }

        public async Task AddContact(Contact contact)
        {
            await _ctx.Contacts.AddAsync(contact);
            _ctx.SaveChanges();
        }

        public async Task<List<Contact>> GetAll()
        {
            return await _ctx.Contacts
                .Include(x => x.Lists)
                .ThenInclude(y => y.List)
                .ToListAsync();
        }

        public async Task RemoveContacts(List<Contact> contacts)
        {
            _ctx.Contacts.RemoveRange(contacts);
            await _ctx.SaveChangesAsync();
        }
    }
}