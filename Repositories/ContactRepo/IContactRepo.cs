using System.Collections.Generic;
using System.Threading.Tasks;
using Models.SmsGateway.ContactModel;

namespace Repositories.ContactRepo
{
    public interface IContactRepo
    {
        Task<List<Contact>> GetAll();
        Task AddContact(Contact contact);
        Task RemoveContacts(List<Contact> contacts);
    }
}