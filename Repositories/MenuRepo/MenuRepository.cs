﻿using Microsoft.EntityFrameworkCore;
using Models.Context;
using SmsGatewayAPI.Models.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Repositories.MenuRepo
{
    public class MenuRepository : IMenuRepository
    {
        private readonly TikiTakaSmsContext _context;

        public MenuRepository(TikiTakaSmsContext context)
        {
            _context = context;
        }

        public async Task<IList<MenuCategory>> GetCategories()
        {
            return await _context.MenuCategories.ToListAsync();
        }
    }
}
