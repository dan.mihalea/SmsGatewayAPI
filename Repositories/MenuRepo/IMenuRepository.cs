﻿using SmsGatewayAPI.Models.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmsGatewayAPI.Repositories.MenuRepo
{
    public interface IMenuRepository
    {
        Task<IList<MenuCategory>> GetCategories();
    }
}
